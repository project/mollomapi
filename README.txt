Mollom API module
===================

Use this module to interface with the Mollom backend API as documented at
http://mollom.com/api. Because the Mollom module provides generic access to
the Mollom Client API, we are focused on the Mollom Reseller APIs with some
overlap due to the shared need to do authentication and error handling.

Check the branches of this module for versions released for specific Drupal
versions.
